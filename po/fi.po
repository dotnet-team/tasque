# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-09-26 16:18+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/tasque.desktop.in.h:1
msgid "Tasque"
msgstr ""

#: ../data/tasque.desktop.in.h:2
msgid "Easy quick task management"
msgstr ""

#: ../data/tasque.desktop.in.h:3
msgid "Task Manager"
msgstr ""

#: ../src/Gtk.Tasque/AllCategory.cs:28
#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:174
msgid "All"
msgstr ""

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:165
#: ../src/Gtk.Tasque/Utilities.cs:282
msgid "Yesterday"
msgstr ""

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:167
msgid "Last 7 Days"
msgstr ""

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:169
msgid "Last Month"
msgstr ""

#: ../src/Gtk.Tasque/CompletedTaskGroup.cs:171
msgid "Last Year"
msgstr ""

#: ../src/Gtk.Tasque/GtkTray.cs:82
#, csharp-format
msgid "{0} task is Overdue\n"
msgid_plural "{0} tasks are Overdue\n"
msgstr[0] ""
msgstr[1] ""

#: ../src/Gtk.Tasque/GtkTray.cs:91
#, csharp-format
msgid "{0} task for Today\n"
msgid_plural "{0} tasks for Today\n"
msgstr[0] ""
msgstr[1] ""

#: ../src/Gtk.Tasque/GtkTray.cs:100
#, csharp-format
msgid "{0} task for Tomorrow\n"
msgid_plural "{0} tasks for Tomorrow\n"
msgstr[0] ""
msgstr[1] ""

#. Translators: This is the status icon's tooltip. When no tasks are overdue, due today, or due tomorrow, it displays this fun message
#: ../src/Gtk.Tasque/GtkTray.cs:106
msgid "Tasque Rocks"
msgstr ""

#: ../src/Gtk.Tasque/GtkTray.cs:149
msgid "translator-credits"
msgstr ""

#: ../src/Gtk.Tasque/GtkTray.cs:157
msgid "Copyright © 2008 Novell, Inc."
msgstr ""

#: ../src/Gtk.Tasque/GtkTray.cs:158
msgid "A Useful Task List"
msgstr ""

#: ../src/Gtk.Tasque/GtkTray.cs:160
msgid "Tasque Project Homepage"
msgstr ""

#: ../src/Gtk.Tasque/GtkTray.cs:173
msgid "New Task ..."
msgstr ""

#: ../src/Gtk.Tasque/GtkTray.cs:183
msgid "Refresh Tasks ..."
msgstr ""

#: ../src/Gtk.Tasque/GtkTray.cs:189
msgid "Toggle Task Window"
msgstr ""

#: ../src/Gtk.Tasque/NoteDialog.cs:24
#, csharp-format
msgid "Notes for: {0:s}"
msgstr ""

#. Update the window title
#: ../src/Gtk.Tasque/PreferencesDialog.cs:97
msgid "Tasque Preferences"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:116
msgid "General"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:124
msgid "Appearance"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:160
msgid "Color Management"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:171
msgid "Today:"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:199
msgid "Overdue:"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:243
msgid "Task Management System"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:285
msgid "Task Filtering"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:304
msgid "Sh_ow completed tasks"
msgstr ""

#. Categories TreeView
#: ../src/Gtk.Tasque/PreferencesDialog.cs:313
msgid "Only _show these categories when \"All\" is selected:"
msgstr ""

#: ../src/Gtk.Tasque/PreferencesDialog.cs:331
msgid "Category"
msgstr ""

#: ../src/Gtk.Tasque/RemoteControl.cs:150
msgid "New task created."
msgstr ""

#. Title for Completed/Checkbox Column
#: ../src/Gtk.Tasque/TaskTreeView.cs:90 ../src/Gtk.Tasque/TaskWindow.cs:343
msgid "Completed"
msgstr ""

#. Title for Priority Column
#: ../src/Gtk.Tasque/TaskTreeView.cs:107
msgid "Priority"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:120 ../src/Gtk.Tasque/TaskTreeView.cs:424
#: ../src/Gtk.Tasque/TaskTreeView.cs:673
msgid "1"
msgstr ""

#. High
#: ../src/Gtk.Tasque/TaskTreeView.cs:121 ../src/Gtk.Tasque/TaskTreeView.cs:421
#: ../src/Gtk.Tasque/TaskTreeView.cs:671
msgid "2"
msgstr ""

#. Medium
#: ../src/Gtk.Tasque/TaskTreeView.cs:122 ../src/Gtk.Tasque/TaskTreeView.cs:418
#: ../src/Gtk.Tasque/TaskTreeView.cs:669
msgid "3"
msgstr ""

#. Low
#: ../src/Gtk.Tasque/TaskTreeView.cs:123 ../src/Gtk.Tasque/TaskTreeView.cs:427
msgid "-"
msgstr ""

#. Title for Task Name Column
#: ../src/Gtk.Tasque/TaskTreeView.cs:137
msgid "Task Name"
msgstr ""

#. Title for Due Date Column
#: ../src/Gtk.Tasque/TaskTreeView.cs:179
msgid "Due Date"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:193 ../src/Gtk.Tasque/TaskTreeView.cs:195
#: ../src/Gtk.Tasque/TaskTreeView.cs:207 ../src/Gtk.Tasque/TaskTreeView.cs:755
#: ../src/Gtk.Tasque/TaskTreeView.cs:758 ../src/Gtk.Tasque/TaskTreeView.cs:763
msgid "M/d - "
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:193 ../src/Gtk.Tasque/TaskTreeView.cs:755
#: ../src/Gtk.Tasque/TaskWindow.cs:276 ../src/Gtk.Tasque/Utilities.cs:276
msgid "Today"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:195 ../src/Gtk.Tasque/TaskTreeView.cs:758
#: ../src/Gtk.Tasque/TaskWindow.cs:294 ../src/Gtk.Tasque/Utilities.cs:295
msgid "Tomorrow"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:197 ../src/Gtk.Tasque/TaskTreeView.cs:199
#: ../src/Gtk.Tasque/TaskTreeView.cs:201 ../src/Gtk.Tasque/TaskTreeView.cs:203
#: ../src/Gtk.Tasque/TaskTreeView.cs:205 ../src/Gtk.Tasque/TaskTreeView.cs:495
#: ../src/Gtk.Tasque/TaskTreeView.cs:772
msgid "M/d - ddd"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:207 ../src/Gtk.Tasque/TaskTreeView.cs:763
msgid "In 1 Week"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:208 ../src/Gtk.Tasque/TaskTreeView.cs:760
#: ../src/Gtk.Tasque/Utilities.cs:308
msgid "No Date"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:209 ../src/Gtk.Tasque/TaskTreeView.cs:765
msgid "Choose Date..."
msgstr ""

#. Title for Notes Column
#: ../src/Gtk.Tasque/TaskTreeView.cs:225
msgid "Notes"
msgstr ""

#. Title for Timer Column
#: ../src/Gtk.Tasque/TaskTreeView.cs:242
msgid "Timer"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:336
msgid "Task Completed"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:497
msgid "M/d/yy - ddd"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:655
msgid "Action Canceled"
msgstr ""

#: ../src/Gtk.Tasque/TaskTreeView.cs:947
#, csharp-format
msgid "Completing Task In: {0}"
msgstr ""

#. The new task entry widget
#. Clear the entry if it contains the default text
#: ../src/Gtk.Tasque/TaskWindow.cs:149 ../src/Gtk.Tasque/TaskWindow.cs:838
#: ../src/Gtk.Tasque/TaskWindow.cs:894 ../src/Gtk.Tasque/TaskWindow.cs:904
#: ../src/Gtk.Tasque/TaskWindow.cs:915
msgid "New task..."
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:165
msgid "_Add"
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:170
msgid "_Add Task"
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:258
msgid "Overdue"
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:312
msgid "Next 7 Days"
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:328
msgid "Future"
msgstr ""

#. Translators: This status shows the date and time when the task list was last refreshed
#: ../src/Gtk.Tasque/TaskWindow.cs:475 ../src/Gtk.Tasque/TaskWindow.cs:1241
#, csharp-format
msgid "Tasks loaded: {0}"
msgstr ""

#. Show error status
#: ../src/Gtk.Tasque/TaskWindow.cs:802
msgid "Error creating a new task"
msgstr ""

#. Show successful status
#: ../src/Gtk.Tasque/TaskWindow.cs:806
msgid "Task created successfully"
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:1090
msgid "_Notes..."
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:1097
msgid "_Delete task"
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:1102
msgid "_Edit task"
msgstr ""

#. TODO Needs translation.
#: ../src/Gtk.Tasque/TaskWindow.cs:1134
msgid "_Change category"
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:1190
msgid "Task deleted"
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:1232
msgid "Loading tasks..."
msgstr ""

#: ../src/Gtk.Tasque/TaskWindow.cs:1253
msgid "Not connected."
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:274
#, csharp-format
msgid "Today, {0}"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:280
#, csharp-format
msgid "Yesterday, {0}"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:286
#, csharp-format
msgid "{0} days ago, {1}"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:288
#, csharp-format
msgid "{0} days ago"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:293
#, csharp-format
msgid "Tomorrow, {0}"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:299
#, csharp-format
msgid "In {0} days, {1}"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:301
#, csharp-format
msgid "In {0} days"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:305
msgid "MMMM d, h:mm tt"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:306
msgid "MMMM d"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:311
msgid "MMMM d yyyy, h:mm tt"
msgstr ""

#: ../src/Gtk.Tasque/Utilities.cs:312
msgid "MMMM d yyyy"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:65
msgid "Click Here to Connect"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:71
#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:142
msgid "You are currently connected"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:75
#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:147
msgid "You are currently connected as"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:79
msgid "You are not connected"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:112
msgid "Remember the Milk not responding. Try again later."
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:119
msgid "Click Here After Authorizing"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:122
msgid "Set the default browser and try again"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:125
msgid "Processing..."
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:135
msgid "Failed, Try Again"
msgstr ""

#: ../src/Addins/Backends/Rtm/Gtk/RtmPreferencesWidget.cs:139
msgid "Thank You"
msgstr ""

#. Token "O". Notice only the suffix is translated.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:49
msgid "th,st,nd,rd"
msgstr ""

#. Token "T"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:53
msgid "today"
msgstr ""

#. Token "T"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:57
msgid "tomorrow"
msgstr ""

#. Token "m". Don't forget to include the plural value, if any
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:61
msgid "month|months"
msgstr ""

#. Token "w". Don't forget to include the plural value, if any
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:65
msgid "week|weeks"
msgstr ""

#. Token "y". Don't forget to include the plural value, if any
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:69
msgid "year|years"
msgstr ""

#. Token "d". Don't forget to include the plural value, if any
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:73
msgid "day|days"
msgstr ""

#. Token "u". More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:207
msgid "due before|due by|due"
msgstr ""

#. Token "n". Examples could be: "Next Month", "Next Monday"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:233
msgid "next"
msgstr ""

#. Token "o". Examples could be: "On April 1st", "On Wednesday"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:239
msgid "on"
msgstr ""

#. Token "i". Examples could be: "In 2 weeks", "In 3 months"
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:245
msgid "in"
msgstr ""

#. Represents "Today and Tomorrow" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:260
msgid "u T"
msgstr ""

#. Represents: "Next" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:263
msgid "n w|n m|n y|n D"
msgstr ""

#. Represents "Due" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:266
msgid "u o D|u M O|u M N|u O|u M"
msgstr ""

#. Represents "On" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:269
msgid "o D|o O"
msgstr ""

#. Represents "In" expression.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:272
msgid "i N d|i d|i N w|i w|i N m|i m|i N y|i y"
msgstr ""

#. Represents all other expressions not using tokens.
#. More information in http://git.gnome.org/browse/tasque/tree/TRANSLATORS
#: ../src/libtasque/TaskParser.cs:275
msgid "T|D|M O|M N|O|A"
msgstr ""
