=head1 NAME

Tasque - Simple TODO list for Gnome

=head1 SYNOPSIS

B<tasque> [OPTION...]

=head1 DESCRIPTION

B<Tasque> is a simple task management application for the Gnome desktop which provides the ability to to tie into any of the following:

=over 4

=item * Remember The Milk (RTM) - Default backend with online accessibility

=item * SQLite - Stand-alone local store

=back

=head1 OPTIONS

=head2 Help options

=over 8

=item B<-?>, B<--help>

Show this help message

=item B<--usage>

Display brief usage message

=back

=head2 GTK+

=over 8

=item B<--gdk-debug=>I<FLAGS>

Gdk debugging flags to set

=item B<--gdk-no-debug=>I<FLAGS>

Gdk debugging flags to unset

=item B<--display=>I<DISPLAY>

X display to use

=item B<--screen=>I<SCREEN>

X screen to use

=item B<--sync>

Make X calls synchronous

=item B<--name=>I<NAME>

Program name as used by the window manager

=item B<--class=>I<CLASS>

Program class as used by the window manager

=item B<--gtk-debug=>I<FLAGS>

Gtk+ debugging flags to set

=item B<--gtk-no-debug=>I<FLAGS>

Gtk+ debugging flags to unset

=item B<--g-fatal-warnings>

Make all warnings fatal

=item B<--gtk-module=>I<MODULE>

Load an additional Gtk module

=back

=head2 Bonobo activation Support

=over 8

=item B<--oaf-ior-fd=>I<FD>

File descriptor to print IOR on

=item B<--oaf-activate-iid=>I<IID>

IID to activate

=item B<--oaf-private>

Prevent registering of server with OAF

=back

=head2 GNOME Library

=over 8

=item B<--disable-sound>

Disable sound server usage

=item B<--enable-sound>

Enable sound server usage

=item B<--espeaker=>I<HOSTNAME:PORT>

Host:port on which the sound server to use is running

=item B<--version>

2.22.0

=back

=head2 Session management

=over 8

=item B<--sm-client-id=>I<ID>

Specify session management ID

=item B<--sm-config-prefix=>I<PREFIX>

Specify prefix of saved configuration

=item B<--sm-disable>

Disable connection to session manager

=back

=head2 GNOME GUI Library

=over 8

=item B<--disable-crash-dialog>

Disable Crash Dialog

=back

=head1 BUGS

Please report bugs at http://bugzilla.gnome.org/browse.cgi?product=tasque.

=head1 AUTHORS

B<Tasque> was written by Boyd Timothy <btimothy@gmail.com>, Calvin Gaisford
<calvinrg@gmail.com>, Brian Merrell <bgmerrell@gmail.com>, and Sandy Armstrong
<sanfordarmstrong@gmail.com>.

This manual page was written by Scott Wegner <swegner2@gmail.com>.

=head1 COPYRIGHT

Copyright (c) 2008 Novell, Inc.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

=cut


